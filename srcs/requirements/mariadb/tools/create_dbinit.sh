#!/bin/sh

# Create a SQL script to init the databases and users
cat << EOF > /tools/dbinit.sql
CREATE DATABASE $MYSQL_DB_NAME;
CREATE USER '$MYSQL_DB_ADMIN'@'%' IDENTIFIED BY '$MYSQL_DB_ADMIN_PASSWORD';
GRANT ALL PRIVILEGES ON *.* TO  '$MYSQL_DB_ADMIN'@'%';
FLUSH PRIVILEGES;
update mysql.user set plugin = 'mysql_native_password' where user='root';
FLUSH PRIVILEGES;
CREATE USER '$MYSQL_DB_USER'@'%' IDENTIFIED BY '$MYSQL_DB_PASSWORD';
GRANT SELECT ON $MYSQL_DB_NAME.* TO '$MYSQL_DB_USER'@'%';
FLUSH PRIVILEGES;
EOF
