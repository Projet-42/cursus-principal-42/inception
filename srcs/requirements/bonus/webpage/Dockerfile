FROM alpine:3.13.4

ARG DOMAIN_NAME

# Download nginx and openssl
RUN apk update \
    && apk add nginx \
    && apk add openssl

# Create a user and a groupe for nginx called 'www'
RUN adduser -D -g 'www' www \
    && mkdir /www \
    && chown -R www:www /var/lib/nginx \
    && chown -R www:www /www \
    && mkdir /run/nginx/

# Create the ssl key and cert for TLS protocole
RUN openssl req -x509 -nodes -days 365 -newkey rsa:2048 \
-subj "/CN=${DOMAIN_NAME}" \
-keyout /etc/ssl/nginx.key -out /etc/ssl/nginx.crt

# Config nginx
COPY ./conf/nginx.conf /etc/nginx/
COPY ./tools/index.html /var/www/site-pages/

EXPOSE 3001

# Run nginx as the main process of the container PID 1
CMD ["nginx", "-g", "daemon off;"]