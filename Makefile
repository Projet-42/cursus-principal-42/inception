compose_file = ./srcs/docker-compose.yml

Env_flag = ./srcs/.env

all: up

up:
	docker-compose -f $(compose_file) --env-file $(Env_flag) up -d --build

build: 
	docker-compose -f $(compose_file) --env-file $(Env_flag) build

stop:
	docker-compose -f $(compose_file) --env-file $(Env_flag) stop

down:
	docker-compose -f $(compose_file) --env-file $(Env_flag) down

images:
	docker images

clean: down
	docker container prune

fclean: clean
	docker rmi srcs_wordpress
	docker rmi srcs_nginx
	docker rmi srcs_mariadb
	docker rmi srcs_adminer
	docker rmi srcs_webpage
	docker rmi debian:buster
	docker rmi alpine:3.13.4
	docker volume rm srcs_wp
	docker volume rm srcs_db

re: fclean all
