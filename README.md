# Inception

- [Inception](#inception)
  - [The project](#the-project)
    - [Things to know](#things-to-know)
  - [Todo list](#todo-list)
  - [Docker commands utils](#docker-commands-utils)
  - [Ressources](#ressources)

## The project

This project will consist in having you set up a mini-infrastructure of different services by following specific rules. This entire project is to be carried out in a virtual machine. To do this, you will have to use docker-compose. 

### Things to know

- [x] Run docker from a makefile
- [x] Docker-compose
- [x] The yml
- [x] Volumes
- [x] .env files 
- [x] TSLv
- [x] Setup the database

## Todo list

- [x] Makefile
- [x] A docker NGINX container with TLSv1.2 or TLSv1.3 only
  - [x] Install nginx
  - [x] Install TSLv
  - [x] configure nginx to listen on 443 port
  - [x] Domain name nabitbol.42.fr
- [x] Setup A docker WordPress + php-fpm container only without
- [x] Setup A docker MariaDB container only without
- [x] Setup A volume containing the Wordpress database
- [x] Setup A volume containg Wordpress site files
- [x] Setup A docker-network linking containers together
- [x] Create .env file
- [x] Containers restart if they crash

## Docker commands utils

- RUN docker: (as the daemon)

> sudo systemctl start docker

- List the images:

> sudo docker images

- List the runing containers:
  
> sudo docker ps

- List all the containers:

> sudo docker ps -a 

- Delete image:

> sudo docker rmi

- Delete all stoped containers:

> sudo docker conteneur prune

- Run a container:

> sudo docker run -it (mod interactive with tty) -d(daemon ord detache the container/run in background)

- Access to the shell of the container:

> sudo docker exec [container name] /bin/sh

- Exit the container shell:

> exit

- Run docker-compose:

> docker-compose up

## Ressources

- Simialr project with docker-compose

`https://medium.com/swlh/wordpress-deployment-with-nginx-php-fpm-and-mariadb-using-docker-compose-55f59e5c1a`

- Nginx 

and TLSv (opensssl)

`https://blog.ipswitch.com/how-to-use-openssl-to-generate-certificates`
`https://nginx.org/en/docs/http/configuring_https_servers.html`

alpine linux

`https://wiki.alpinelinux.org/wiki/Nginx`

- Mariadb

`https://mariadb.com/kb/en/mysqld_safe/`
`https://www.digitalocean.com/community/tutorials/how-to-install-mariadb-on-debian-10`
`https://phoenixnap.com/kb/how-to-create-mariadb-user-grant-privileges`

- Wordpress
  
`https://developer.wordpress.org/cli/commands/config/create/`

- Doc docker, docker-compose and dockerfile

`https://cours.brosseau.ovh/tp/docker/docker_compose.html`
`https://docs.docker.com/engine/reference/builder/#cmd`
`https://docs.docker.com/develop/develop-images/dockerfile_best-practices/#exclude-with-dockerignore`
`https://docs.docker.com/engine/reference/builder/#dockerignore-file`
`https://docs.docker.com/compose/`

- Environnement

`https://docs.docker.com/compose/environment-variables/`

- Adminer

`https://github.com/dockette/adminer`